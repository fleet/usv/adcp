import time
import traceback

import adcp_drone.serial_command.serial_interface as ser


def run_something():
    comm = ser.Communicator(ser.SerialLoop())

    msg_out = "Hello, How are you"

    comm.open()
    comm.write(msg_out)
    time.sleep(2)
    comm.close()
    # assume that everything went well
    assert (True)

if __name__ == '__main__':
    run_something()
