import time
import unittest
import adcp_drone.serial_command.serial_interface as ser

class MyTestCase(unittest.TestCase):
    def test_basic_loop(self):
        comm = ser.Communicator(ser.SerialLoop())

        msg_out = "Hello, How are you"

        comm.open()
        comm.write(msg_out)
        time.sleep(2)
        comm.close()
        #assume that everything went well
        assert(True)

    def test_context_mgr(self):
        msg_out = "Hello, How are you"
        with ser.open_communicator(ser.SerialLoop()) as com:
            com.write(msg_out)
            time.sleep(2)
        #assume that everything went well
        assert(True)


if __name__ == '__main__':
    MyTestCase().test_context_mgr()
    MyTestCase().test_basic_loop()
