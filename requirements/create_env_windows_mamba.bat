REM install mamba if not already done
call conda install mamba -c conda-forge -y

echo mamba env remove --name pyadcp -y
call mamba env remove --name pyadcp -y

echo mamba env create -f requirements.yml --name pyadcp
call mamba env create -f requirements.yml --name pyadcp

