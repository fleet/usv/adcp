# pyNvi

Python code repository related to NVI format

## How to use

. Install pynvi in your environment with PIP

```
pip install pynvi --extra-index-url https://gitlab.ifremer.fr/api/v4/projects/2736/packages/pypi/simple
```

. Now you can use it
```
import pynvi
with nvi.open_nvi(NVI_PATH) as f:
    lat = f.get_latitudes()
....

```

## Deploy new release

pyNvi project can be pip installable. Use the following commands (with gitlab-pynvi pointing to package repository):

```
python -m build
python -m twine upload --repository gitlab_pynvi dist/*
```


Note : id of server is defined in ~/.pypirc.


