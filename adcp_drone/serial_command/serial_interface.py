import abc
from contextlib import contextmanager
from typing import Generator

import serial
import traceback
import logging

from serial.threaded import ReaderThread

FORMAT = '%(asctime)s %(message)s'
logging.basicConfig(format=FORMAT,level=logging.INFO)


class Logger:
    def info(message):
        logging.info(message)

    def error(message):
        logging.error(message)

    def warning(message):
        logging.warning(message)


class PrintLines(serial.threaded.LineReader):
    def connection_made(self, transport):
        super(PrintLines, self).connection_made(transport)
        Logger.info('connexion opened')

    def handle_line(self, data):
        Logger.info(f'received message:[{repr(data)}]')

    def connection_lost(self, exc):
        if exc:
            for tr in traceback.format_stack():
                Logger.error(tr)

            Logger.info('connexion closed\n')

class SerialProtocol(abc.ABC):
    """
    Base class for serial interface, allow to write messages to serial port for ADCP.
    Received messages are written to log files and/or console output
    """
    @abc.abstractmethod
    def get_protocol(self) -> serial.Serial:
        """ return the instanciated protocol (instance of serial.Serial"""

    def send_break(self,duration:float):
        """send a break signal"""
        self.get_protocol().send_break(duration=duration)


class SerialLoop(SerialProtocol):
    def __init__(self):
        """"""
        self.serial = None
        self.serial = serial.serial_for_url("loop://",do_not_open=True)
        self.serial.timeout=100

    def get_protocol(self) -> serial.Serial:
        return self.serial

class SerialRDI(SerialProtocol):
    def __init__(self, baudrate=9600, port='COM10', parity=serial.PARITY_NONE,
                 stop_bit=serial.STOPBITS_ONE):  # default settings for RDI ADCP
        self.serial = serial.Serial()
        self.serial.baudrate = baudrate
        self.serial.port = port
        self.serial.parity = parity
        self.serial.stop_bit = stop_bit

    def get_protocol(self) -> serial.Serial:
        return self.serial


class Communicator():
    """Communicator allow to talk on serial port, a thread is created for serial port reading and dispatch"""

    def __init__(self, serial_protocol: SerialProtocol, ):
        self.protocol = None
        self.serial_protocol = serial_protocol
        self.reader_thread = ReaderThread(serial_protocol.get_protocol(), PrintLines)

    def send_break(self, duration_sec: float):
        with(self.reader_thread._lock):
            self.serial_protocol.get_protocol().send_break(duration=duration_sec)

    def open(self):
        """Open serial port and start reading"""
        self.serial_protocol.get_protocol().open()
        self.reader_thread.start()
        transport, protocol = self.reader_thread.connect()
        self.protocol = protocol

    def write(self, msg):
        """write a message to serial port"""
        Logger.info(f"write message {msg}")
        self.protocol.write_line(msg) # use reader_thread to have a lock mechanism

    def close(self):
        """close thread and connection"""
        self.reader_thread.close()
        self.serial_protocol.get_protocol().close()




@contextmanager
def open_communicator(serial_protocol : SerialProtocol) -> Generator[Communicator, None, None]:
    com = Communicator(serial_protocol=serial_protocol)
    com.open()
    try:
        yield com
    finally:
        com.close()
