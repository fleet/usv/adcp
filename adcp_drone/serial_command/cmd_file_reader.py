from adcp_drone.serial_command.serial_interface import Communicator


class CmdReader:
    """
    Send command to serial ADCP from a text file

    """
    def __init__(self, communicator: Communicator):
        self.COMMENT_MARKER = ";"
        self.communicator = communicator
        pass

    def read(self,filename:str):
        # Using for loop
        with  open(filename, 'r') as file:
            for line in file:
                line = line.strip() #remove \n
                #remove comments
                line_split = line.split(self.COMMENT_MARKER)
                line = line_split[0].strip()
                line = line.strip()
                if len(line) > 0:
                    #send data to
                    self.communicator.write(line)


if __name__ == '__main__':
    # pylint: disable=wrong-import-position
    import os
    import time
    import adcp_drone.serial_command.serial_interface as ser

    with ser.open_communicator(ser.SerialRDI(baudrate=38400, port='COM8')) as com:
        reader = CmdReader(communicator=com)
        reader.communicator.send_break(duration_sec=3)
       # filename = os.path.abspath("../../tests/0-35m_ADCP_Europe.txt")
        filename = os.path.abspath("../../tests/ADCP_DrixLight.txt")
        filename2 = os.path.realpath(filename)
        reader.read(filename2)
        time.sleep(5)
        # assume that everything went well
    print("End of communication")
